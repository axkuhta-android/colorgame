package com.example.colorgame

import android.app.AlertDialog
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import java.lang.Math.random

data class Coord(val x:Int, val y:Int)

class MainActivity : AppCompatActivity() {
    lateinit var tiles: Array<Array<View>>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tiles = arrayOf(
            arrayOf(
                findViewById(R.id.t00),
                findViewById(R.id.t01),
                findViewById(R.id.t02),
                findViewById(R.id.t03)
            ),
            arrayOf(
                findViewById(R.id.t10),
                findViewById(R.id.t11),
                findViewById(R.id.t12),
                findViewById(R.id.t13)
            ),
            arrayOf(
                findViewById(R.id.t20),
                findViewById(R.id.t21),
                findViewById(R.id.t22),
                findViewById(R.id.t23)
            ),
            arrayOf(
                findViewById(R.id.t30),
                findViewById(R.id.t31),
                findViewById(R.id.t32),
                findViewById(R.id.t33)
            )
        )

        for (row in tiles) {
            for (tile in row) {
                tile.setOnClickListener {
                    onClick(it)
                }
            }
        }

        randomInit()
    }

    fun randomInit() {
        for (row in tiles) {
            for (tile in row) {
                if (random() > 0.5)
                    changeColor(tile)
            }
        }
    }

    fun getCoordFromString(str: String):Coord {
        val split = str.split(" ")
        return Coord(str[1].toString().toInt(), str[0].toString().toInt())
    }

    fun changeColor(v: View) {
        val brightColor = resources.getColor(R.color.bright)
        val darkColor = resources.getColor(R.color.dark)
        val drawable = v.background as ColorDrawable
        if (drawable.color ==brightColor ) {
            v.setBackgroundColor(darkColor)
        } else {
            v.setBackgroundColor(brightColor)
        }
    }

    fun onClick(v: View) {
        val coord = getCoordFromString(v.tag.toString())
        changeColor(v)

        for (i in 0..3) {
            changeColor(tiles[coord.y][i])
            changeColor(tiles[i][coord.x])
        }

        winConditionCheck()
    }

    fun winConditionCheck() {
        val brightColor = resources.getColor(R.color.bright)
        var tilesSet = 0
        var tilesClear = 0

        for (row in tiles) {
            for (tile in row) {
                val drawable = tile.background as ColorDrawable
                if (drawable.color == brightColor) {
                    tilesSet++
                } else {
                    tilesClear++
                }
            }
        }

        if (tilesSet == 16 || tilesClear == 16) {
            AlertDialog.Builder(this)
                .setTitle("You won!")
                .setMessage("Begin again?")
                .setPositiveButton("Yes") {
                        dialog, which -> randomInit()
                }
                .show()
        }
    }
}